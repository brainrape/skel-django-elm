from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.timezone import now
from django.shortcuts import render
from django.views.generic import View
from rest_framework import permissions, status, generics
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import AuthenticationFailed, ValidationError

from account import models, serializers
from app.utils.email import send_mail


import logging; logger = logging.getLogger('django')



def get_user_data(user, token):
    serializer = serializers.UserSerializer(user)
    data = serializer.data
    data['token'] = token.key
    return Response(data)


class Users(generics.ListAPIView):
    permission_classes = (permissions.IsAdminUser, )
    serializer_class = serializers.UserSerializer
    queryset = models.User.objects.all()


class Account(generics.RetrieveUpdateAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.UserSerializer

    def get_object(self):
        return self.request.user

    def get(self, request):
        try:
            token = Token.objects.get(user=request.user)
        except Token.DoesNotExist:
            raise AuthenticationFailed()
        return get_user_data(request.user, token)


class Login(APIView):
    serializer_class = serializers.AuthTokenSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            return get_user_data(user, token)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Logout(APIView):
    def post(self, request):
        if request.auth is not None:
            request.auth.delete()
        return Response()


class ChangePassword(APIView):
    serializer_class = serializers.ChangePasswordSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        serializer = self.serializer_class(
            user=request.user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeleteAccount(APIView):
    serializer_class = serializers.DeleteAccountSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        serializer = self.serializer_class(
            user=request.user, data=request.data)
        if serializer.is_valid():
            request.user.is_active = False
            request.user.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Signup(APIView):
    serializer_class = serializers.SignupSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                user.set_password(serializer.validated_data.get('password1'))
                user.save()

                activation = models.Activation.create_activation(user)
                send_activation_email(activation)
                token, created = Token.objects.get_or_create(user=user)

                serializer = serializers.UserSerializer(user)
                return Response({**serializer.data, 'token': token.key}, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
