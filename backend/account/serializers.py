from django.contrib.auth import authenticate, login
from django.contrib.auth.password_validation import validate_password
from rest_framework import fields
from rest_framework import serializers

from account.models import User
from app.serializers import TimestampField


LOGIN_WRONG_CREDENTIALS = 'Email and password do not match.'
LOGIN_MISSING_CREDENTIALS = 'Missing credentials.'
PASSWORD_MISMATCH = 'The passwords don\'t match.'
EMAIL_IN_USE = 'This email is already in use.'
PASSWORD_OLD_INCORRECT = 'The old password is incorrect.'
EMAIL_MISMATCH = 'The emails don\'t match.'
PASSWORD_INCORRECT = 'The password is incorrect.'
CONFIRM_INCORRECT = 'Please type the phrase above.'


class UserSerializer(serializers.ModelSerializer):
    created = TimestampField(read_only=True)
    updated = TimestampField(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'email_verified', 'is_active', 'is_admin',  'created', 'updated')
        read_only_fields = ('id', 'email', 'email_verified', 'is_active', 'is_admin', 'created', 'updated')


class AuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        email = attrs.get('email', None)
        password = attrs.get('password', None)

        if email and password:
            user = authenticate(email=email, password=password)
            if user:
                attrs['user'] = user
                return attrs
            else:
                raise serializers.ValidationError(LOGIN_WRONG_CREDENTIALS)
        else:
            raise serializers.ValidationError(LOGIN_MISSING_CREDENTIALS)


class PasswordSerializer(serializers.Serializer):
    password1 = fields.CharField()
    password2 = fields.CharField()

    def validate_password1(self, value):
        validate_password(value)
        return value

    def validate(self, attrs):
        if 'password1' in attrs and 'password2' in attrs:
            if attrs['password1'] != attrs['password2']:
                raise serializers.ValidationError(PASSWORD_MISMATCH)
        return attrs


class ChangePasswordSerializer(PasswordSerializer):
    old_password = serializers.CharField(label='Old password')

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ChangePasswordSerializer, self).__init__(*args, **kwargs)

    def create(self, validated_data):
        self.user.set_password(validated_data['password1'])
        self.user.save()
        return self.user

    def validate_old_password(self, value):
        if not self.user.check_password(value):
            raise serializers.ValidationError(PASSWORD_OLD_INCORRECT)
        return value


class ChangeEmailSerializer(serializers.Serializer):
    email1 = serializers.EmailField(required=True)
    email2 = serializers.EmailField(required=True)
    password = serializers.CharField(label='Password')

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ChangeEmailSerializer, self).__init__(*args, **kwargs)

    def validate(self, attrs):
        if 'email1' in attrs and 'email2' in attrs:
            if attrs['email1'] != attrs['email2']:
                raise serializers.ValidationError(EMAIL_MISMATCH)
        return attrs

    def create(self, validated_data):
        self.user.email = validated_data['email1']
        self.user.save()
        return self.user

    def validate_password(self, value):
        if not self.user.check_password(value):
            raise serializers.ValidationError(PASSWORD_INCORRECT)
        return value


class DeleteAccountSerializer(serializers.Serializer):
    confirmation = serializers.CharField(required=True)
    password = serializers.CharField(label='Password')

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(DeleteAccountSerializer, self).__init__(*args, **kwargs)

    def validate_confirmation(self, value):
        if value != 'delete my account':
            raise serializers.ValidationError(CONFIRM_INCORRECT)
        return value

    def validate_password(self, value):
        if not self.user.check_password(value):
            raise serializers.ValidationError(PASSWORD_INCORRECT)
        return value


class SignupSerializer(PasswordSerializer):
    email = fields.EmailField()

    def validate_email(self, value):
        try:
            User.objects.get(email=value)
            raise serializers.ValidationError(EMAIL_IN_USE)
        except User.DoesNotExist:
            pass
        return value

    def create(self, validated_data):
        return User(
            email=validated_data.get('email'),)
