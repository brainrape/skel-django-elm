from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from account import views

urlpatterns = [
    url(r'^me/?$',
        views.Account.as_view(), name='account'),
    url(r'^signup/?$',
        views.Signup.as_view(), name='signup'),
    url(r'^login/?$',
        views.Login.as_view(), name='login'),
    url(r'^logout/?$',
        views.Logout.as_view(), name='logout'),
    url(r'^password/?$',
        views.ChangePassword.as_view(), name='change-password'),
    url(r'^delete/?$',
        views.DeleteAccount.as_view(), name='delete-account'),
]
