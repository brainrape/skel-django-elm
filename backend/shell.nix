# nix-shell environment for backend development

{ pkgs ? import ../nixpkgs-pinned.nix {}, ...} :
pkgs.lib.overrideDerivation (import ./default.nix { inherit pkgs; }) (old :
  let
    update-backend-deps = pkgs.writeScriptBin "update-backend-deps" ''
      #!${pkgs.stdenv.shell}
      echo "Pinning python dependendencies from ./requirements.txt ..."
      ${pkgs.pypi2nix}/bin/pypi2nix -V 3.6 -r requirements.txt -E libxml2 -E libxslt -E libjpeg -E postgresql
    '';
    dj = pkgs.writeScriptBin "dj" ''
      #!${pkgs.stdenv.shell}
      django-admin $@
    '';
  in {
    buildInputs = old.propagatedBuildInputs ++ [ pkgs.pypi2nix update-backend-deps dj pkgs.sqlite ];
    shellHook = old.shellHook + ''
      export DJANGO_SETTINGS_MODULE=app.dev_settings
    '';
  }
)
