from rest_framework import generics, mixins, permissions, status, exceptions, renderers
from rest_framework.views import APIView
from item.models import Item
from item.serializers import ItemSerializer


class ItemsView(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = ItemSerializer

    def get_queryset(self):
        return Item.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ItemView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = ItemSerializer
    lookup_field = "uid"

    def get_queryset(self):
        return Product.objects.filter(user=self.request.user)
