from django.conf.urls import url
from item import views

urlpatterns = [
    url(r'^items/(?P<uid>[a-z0-9]+)/?$',
        views.ItemsView.as_view(), name='items'),
]
