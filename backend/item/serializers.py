from django.db import models
from rest_framework import serializers

from app.serializers import TimestampField
from item.models import Item


class ItemSerializer(serializers.ModelSerializer):
    created = TimestampField(read_only=True)
    updated = TimestampField(read_only=True)

    class Meta:
        model = Item
        fields = ('uid', 'user', 'text', 'created', 'updated')
        read_only_fields = ('uid', 'user', 'created', 'updated', )
