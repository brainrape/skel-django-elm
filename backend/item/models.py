from django.db import models
from django.utils.crypto import get_random_string
from account.models import User


def generate_id():
    return get_random_string(12).lower()


class Item(models.Model):
    uid = models.CharField(max_length=12, primary_key=True, default=generate_id, editable=False)
    user = models.ForeignKey(User, models.CASCADE)
    text = models.TextField()

    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)
