# Overrides for python dependencies
{ pkgs, python }:
self: super:
{
  "lxml" = python.overrideDerivation super."lxml" (old: {
    propagatedBuildInputs = [];
  });
  "psycopg2" = python.overrideDerivation super."psycopg2" (old: {
    propagatedBuildInputs = [ pkgs.postgresql ];
  });
  "python-dateutil" = python.overrideDerivation super."python-dateutil" (old: {
    propagatedBuildInputs = [ super.six super.setuptools-scm ];
  });
}
