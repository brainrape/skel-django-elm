from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.views.generic import TemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include('djoser.urls')),
    url(r'^auth/', include('djoser.urls.authtoken')),
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    url(r'^account/', include('account.urls')),
    url(r'^', include('item.urls')),
] + staticfiles_urlpatterns()
