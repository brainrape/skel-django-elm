from app.common_settings import *

# Secrets

if 'DJANGO_DOMAIN' not in os.environ:
    raise Exception('Please set DJANGO_DOMAIN environment variable, eg. "testing.myapp.com"!')
DJANGO_DOMAIN = os.environ['DJANGO_DOMAIN']

if 'DJANGO_SECRET_KEY' not in os.environ:
   raise Exception('Please set DJANGO_SECRET_KEY environment variable, eg. run `openssl rand -base64 32`!')
SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

if 'DJANGO_SECRET_EXAMPLE' not in os.environ:
   raise Exception('Please set DJANGO_SECRET_EXAMPLE environment variable!')
SECRET_EXAMPLE = os.environ['DJANGO_SECRET_EXAMPLE']


# App

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'app',
    }
}

PROTOCOL = 'https'
FORCE_SCRIPT_NAME = '/data'
ALLOWED_HOSTS = [ '.' + DJANGO_DOMAIN, ]

CORS_ORIGIN_WHITELIST = [ DJANGO_DOMAIN, ]
CORS_ALLOW_CREDENTIALS = True

STATIC_URL = '/static/'
MEDIA_ROOT = '/data/uploads'


# Email

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'myapp.test@gmail.com'
EMAIL_USE_TLS = True
EMAIL_PORT = 587
DJOSER['DOMAIN'] = DJANGO_DOMAIN  # Password reset email domain
