import logging

from smtplib import SMTPRecipientsRefused
from django.core.mail import EmailMultiAlternatives

from django.conf import settings
from django.template import loader

logger = logging.getLogger(__name__)


def send_mail(subject_template_name, email_template_name, to_email,
              context, from_email=settings.DEFAULT_FROM_EMAIL):
    try:
        subject = loader.render_to_string(subject_template_name, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        if not isinstance(to_email, list):
            to_email = [to_email]

        email_message = EmailMultiAlternatives(
            subject, body, from_email, to_email)

        email_message.send()

    except SMTPRecipientsRefused as e:
        logger.error(e)

    except Exception as e:
        message = 'Failed to send email to {}, Subject: {}, Exception: {}'.format(
            to_email, subject, e)
        logger.exception(message)


def send_activation_email(activation):
    activation_link = '{}://{}/verify/{}'.format(
        settings.PROTOCOL,
        settings.FRONTEND_DOMAIN,
        activation.activation_key, )
    context = {
        'activation_link': activation_link, }
    send_mail(
        subject_template_name='emails/verify_email_subject.txt',
        email_template_name='emails/verify_email.txt',
        to_email=activation.user.email,
        context=context, )
