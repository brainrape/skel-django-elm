#!/usr/bin/env python

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.server_settings")

application = get_wsgi_application()

def main():
  import sys
  sys.argv = ["gunicorn", "app.wsgi"]
  import gunicorn.app.wsgiapp
  gunicorn.app.wsgiapp.run()

if __name__ == "__main__":
  main()
