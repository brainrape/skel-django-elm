from app.common_settings import *

# Secrets

SECRET_KEY = "0p1s-eblrmozho1g-=68rk(68r5_8ax$w(bl=28ub(tld73x6#"
EMAIL_HOST_PASSWORD=""
STRIPE_API_KEY=""
STRIPE_WEBHOOK_SIGNATURE_SUBSCRIPTION=""
STRIPE_TEST_PUBLIC_KEY=""

# Debug

DEBUG=True
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
STATIC_URL = '/static/'
MEDIA_ROOT = os.path.join(os.path.dirname(os.path.dirname(BASE_DIR)), 'data', 'uploads')


# App

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'PORT': '54321', }, }

PROTOCOL="http"
FORCE_SCRIPT_NAME = ''
ALLOWED_HOSTS = [ "localhost", "localhost:8000", "localhost:8080" ]
CORS_ORIGIN_WHITELIST = ('localhost:8080', )
DJOSER['DOMAIN'] = "localhost"  # Password reset email domain
