#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

setup(
  name='myapp',
  version='1.0',
  description='myapp',
  packages=find_packages(),
  include_package_data=True,
  entry_points={
    'console_scripts': [
      'myapp-manage=app.manage:main',
      'myapp-wsgi=app.wsgi:main'
    ]
  },
)
