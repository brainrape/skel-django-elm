# Nix expression defining the backend derivation

{ pkgs ? import ../nixpkgs-pinned.nix {}} :
let
  python = import ./requirements.nix { inherit pkgs; };
in python.mkDerivation {
  name = "myapp-backend";
  src = ./.;
  doCheck = false;
  propagatedBuildInputs = builtins.attrValues python.packages;
  postInstall = ''
    DJANGO_SETTINGS_MODULE=app.dev_settings python app/manage.py collectstatic --noinput
    ln -s lib/python3.6/site-packages/myapp/static $out/static
  '';
}
