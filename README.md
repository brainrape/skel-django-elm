# skel-django-elm

Skeleton Project with Django, Elm and Nix.


## Setup

[Install Nix](https://nixos.org/nix/download.html) and clone this Git repository.

Find and Replace All in project:
  - `myapp` → your project's name (for machines)
  - `My App` → your project's name (for humans)
  - `brainrape/skel-django-elm` → your GitLab repo
  - `a78CBQSdNRp3WY2o` → new test account password


### Develop

Start development server:

```
nix-shell --run dev
```

When it is ready, a browser window will open. Log in with `testuser@example.com`,  `a78CBQSdNRp3WY2o`.


#### Run django management commands:

```
cd backend
nix-shell --run 'dj help'
```


#### Update Dependencies

```
cd backend
# edit `requirements.txt`
nix-shell --run update-backend-deps
```

```
cd frontend
# edit `node-packages.json` or `elm-package.json`
nix-shell --run update-frontend-deps
```


#### Build and Test

```
nix-shell --run check
```


### Deploy

If you have SSH access to a running server, you can deploy the current `git HEAD` on it:

```
cd infrastructure
nix-shell --run './deploy user@host'
# or specify SSH private key
nix-shell --run './deploy -i private_key user@host'
```


## Provision Cloud Infrastructure

Create a DigitalOcean SSH key, droplet, database volume, floating IP, DNS records, configure the OS and deploy the app:

```
cd infrastructure
# inspect and apply the changes
nix-shell --run 'terraform apply'
```

Terraform will ask you for an environment name (eg. your name, or "staging", or the distinguished value "production"), and a Digital Ocean authentication token. You can [save these variables](https://www.terraform.io/intro/getting-started/variables.html#assigning-variables). Do not commit them into the repo.

In the end it will output the IP address and domain name of the created server.

SSH into the server to look around and run commands:

```
./ssh 'journalctl -u myapp -n 100'
```

You can run `terraform apply` again any time to deploy `git HEAD` to your server.

When you're done, destroy the infrastructure:

```
nix-shell --run 'terraform destroy'
```


## CI/CD

CI Pipelines are automatically run on GitLab CI. If the functional tests fail, Merge Requests cannot be merged.
If you want the repo and PRs on GitHub, you can [set up mirroring](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html).

CI Pipelines:
[https://gitlab.com/brainrape/skel-django-elm/pipelines](https://gitlab.com/brainrape/skel-django-elm/pipelines)

Deployment Environments:
[https://gitlab.com/brainrape/skel-django-elm/environments](https://gitlab.com/brainrape/skel-django-elm/environments)
