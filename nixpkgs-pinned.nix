# Define a pinned nixpkgs version
{ pkgs ? import <nixpkgs> {}, args ? {}, ...}:
let
  pinnedVersion = pkgs.lib.importJSON ./nixpkgs-pinned.json;
in
  import (pkgs.fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs-channels";
    inherit (pinnedVersion) rev sha256;
 }) {}
