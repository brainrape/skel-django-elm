# Nix package that contains frontend and backend

{ pkgs ? import ./nixpkgs-pinned.nix {}, ...} :
let
  frontend = import ./frontend { inherit pkgs; };
  backend = import ./backend { inherit pkgs; };

  nixos-module = import ./infrastructure/app.nix { inherit pkgs; inherit (pkgs) lib; config = {}; };
  postgresql = nixos-module.config.services.postgresql.package;

  # useful commands
  check = pkgs.writeShellScriptBin "check" ''
    set -e
    echo "Running backend tests..."
    cd backend
    # nix-shell --run "app/manage.py test"
    cd ..
    echo "Running frontend tests..."
    cd frontend
    nix-shell --run "npm test"
    cd ..
    echo "Success."
  '';
  dev = pkgs.writeShellScriptBin "dev" ''
    PATH="${pkgs.which}/bin:${pkgs.tmux}/bin:${launch-browser}/bin:$PATH"
    ${initialize}/bin/initialize
    LOCALE_ARCHIVE=${pkgs.glibcLocales}/lib/locale/locale-archive
    rm -f .overmind.sock
    ${pkgs.overmind}/bin/overmind start
  '';
  initialize = pkgs.writeShellScriptBin "initialize" ''
    set -e
    [ -f data/initialized ] && exit
    mkdir -p data/postgres
    ${postgresql}/bin/initdb -D data/postgres
    ${postgresql}/bin/postgres -D data/postgres -p 54321 &
    PID=$!
    echo $PID
    sleep 5
    ${migrate}/bin/migrate
    ${loaddata}/bin/loaddata
    kill $PID
    touch data/initialized
  '';
  launch-browser = pkgs.writeShellScriptBin "launch-browser" ''
    set -e
    while [[ "$(curl -s -o /dev/null -w '%{http_code}' localhost:8000)" != "200" ]]; do sleep 1; done
    while [[ "$(curl -s -o /dev/null -w '%{http_code}' localhost:8080)" != "200" ]]; do sleep 1; done
    sleep 1
    echo; echo; echo;
    echo "Development Server Ready at http://localhost:8000"
    echo;
    echo "Launching browser. You can log in with:"
    echo "  email: testuser@example.com"
    echo "  password: a78CBQSdNRp3WY2o"
    echo;
    ${pkgs.python}/bin/python -mwebbrowser http://localhost:8080 &> /dev/null
    while true; do sleep 1000; done
  '';
  migrate = pkgs.writeShellScriptBin "migrate" ''
    cd backend
    nix-shell --run 'app/manage.py migrate'
  '';
  loaddata = pkgs.writeShellScriptBin "loaddata" ''
    cd backend
    nix-shell --run 'app/manage.py loaddata */fixtures/example.json'
  '';
  dbshell = pkgs.writeShellScriptBin "dbshell" ''
    ${postgresql}/bin/psql -d postgres -p 54321
  '';
  update-nixpkgs = pkgs.writeShellScriptBin "update-nixpkgs" ''
    ${pkgs.nix-prefetch-git}/bin/nix-prefetch-git \
      https://github.com/nixos/nixpkgs-channels.git \
      --rev refs/heads/nixos-18.09 \
      > ./nixpkgs-pinned.json
  '';
in
  pkgs.buildEnv {
    name = "myapp-all";
    buildInputs = [
      pkgs.which
      pkgs.tmux
      pkgs.overmind
      postgresql
      check
      dev
      initialize
      migrate
      loaddata
      dbshell
      launch-browser
      update-nixpkgs
    ];
    paths = [
      frontend
      backend
      check
      dev
      initialize
      migrate
      loaddata
      dbshell
      launch-browser
      update-nixpkgs
    ];
    passthru = {
      inherit frontend backend check dev migrate loaddata dbshell;
    };
  }
