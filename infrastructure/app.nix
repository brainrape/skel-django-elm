# NixOS module defining myapp server

{ config, lib, pkgs, ... } :
let
  cfg = config.services.myapp;
  postgresqlPackage = pkgs.postgresql96;
  frontendPackage = import ../frontend {
    debug = (cfg.env != "production");
    serverUrl = (if cfg.enableSSL then "https" else "http") + "://${cfg.domain}/data/";
  };
  backendPackage = import ../backend { };
  backendWrapper = pkgs.writeShellScriptBin "myapp-manage" ''
    set -e
    set -o allexport
    source /root/SECRET_vars
    set +o allexport
    export DJANGO_DOMAIN="${cfg.domain}"
    /var/run/current-system/sw/bin/sudo -E -i -u myapp \
      ${backendPackage}/bin/myapp-manage "$@"
  '';
in {
  imports = [];

  options.services.myapp = with lib; with lib.types; {
    env = mkOption {
      type = string;
    };

    domain = mkOption {
      type = string;
    };

    enableSSL = lib.mkOption {
      type = bool;
      default = true;
    };

    dataDir = mkOption {
      type = string;
      default = "/data/";
    };
  };

  config = {
    users.users.root.openssh.authorizedKeys.keys = [
      "copy your ssh public key here"
    ];

    networking.extraHosts = "127.0.0.1 ${cfg.domain}";
    networking.firewall.allowedTCPPorts = [ 22 80 443 ];

    environment.systemPackages = [ backendWrapper ];

    users.extraUsers.myapp = { group = "myapp"; };
    users.extraGroups.myapp = {};

    services.nginx = {
      enable = true;
      statusPage = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      recommendedGzipSettings = true;

      virtualHosts."${cfg.domain}" = {
        forceSSL = cfg.enableSSL;
        enableACME = cfg.enableSSL;

        # frontend
        locations."/" = {
          root = "${frontendPackage}/public/";
          tryFiles = "\$uri /index.html";
          extraConfig = "expires -1;";
        };

        # backend
        locations."/data/" = {
          proxyPass = "http://localhost:8000/";
        };

        # static files
        locations."/static/" = {
          root = backendPackage;
          extraConfig = "expires 1h;";
        };
        locations."/data/static/" = {
          root = "${backendPackage}/static";
          extraConfig = ''
            rewrite ^/data/static/(.*)$ /$1 break;
          '';
        };

        # uploaded files
        locations."/data/uploads/" = {
          root = "${cfg.dataDir}/uploads";
          extraConfig = ''
            rewrite ^/data/uploads/(.*)$ /$1 break;
          '';
        };
      };
    };

    systemd.services.myapp = {
      description = "myapp backend";
      wants = [ "network.target" "myapp-create-db.service" ];
      after = [ "network.target" "myapp-create-db.service"];
      wantedBy = [ "multi-user.target" ];
      restartIfChanged = true;
      environment = {
        DJANGO_DOMAIN = cfg.domain;
      };
      serviceConfig = {
        User = "myapp";
        Group = "myapp";
        EnvironmentFile = "/root/SECRET_vars";
        Restart = "always";
        RestartSec = "10s";
        StartLimitInterval = 300;
        StartLimitBurst = 3;
        PermissionsStartOnly = true;
      };
      preStart = ''
        mkdir -p ${cfg.dataDir}/uploads
        chown myapp. ${cfg.dataDir}/uploads
      '';
      script = ''
        ${backendPackage}/bin/myapp-manage migrate --noinput
        ${backendPackage}/bin/myapp-wsgi
      '';
    };

    systemd.services.myapp-create-db = {
      serviceConfig.Type = "oneshot";
      wants = [ "postgresql.service" ];
      after = [ "postgresql.service" ];
      serviceConfig.User = "postgres";
      script = ''
        ${config.services.postgresql.package}/bin/createuser myapp | true
        ${config.services.postgresql.package}/bin/createdb myapp -O myapp | true
      '';
    };

    services.postgresql = {
      enable = true;
      package = postgresqlPackage;
      dataDir = "${cfg.dataDir}/postgresql";
    };

    services.postgresqlBackup = {
      enable = true;
      databases = [ "myapp" ];
      location = "${cfg.dataDir}/postgresql-backup";
    };
  };
}
