# Infrastructure definition

variable "do_token" {
  description = "Digital Ocean access token. See https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2#how-to-generate-a-personal-access-token"
}

variable "env" {
  description = "Name for the infrastructure environment. It will appear in DigitalOcean resource names and domain names. 'staging', 'production', or usually, your username."
}

variable "domain" {
  description = "Existing DNS zone on DigitalOcean. DNS records will be added here."
  default = ""
}

variable "django_secret_example" {
  description = "Example for passing secrets to the backend. Remove the default value and save it outside the repo, see README."
  default = "EXAMPLE"
}

variable "do_region" {
  default = "nyc3"
}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "tls_private_key" "app" {
  algorithm = "RSA"

  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_ssh_key" "app" {
  name       = "myapp-${var.env}"
  public_key = "${tls_private_key.app.public_key_openssh}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_volume" "data" {
  region      = "${var.do_region}"
  name        = "myapp-${var.env}-data"
  size        = "${var.env == "production" ? 100 : 10}"
  description = "Data volume for myapp-${var.env}"

  lifecycle {
    create_before_destroy = true

    # Comment this out if you are sure you want to DESTROY YOUR DATA
    prevent_destroy = true
  }
}

data "template_file" "host-nixos-config" {
  template = <<EOF
# NixOS module for app host system
# Provisioned by Terraform

let
  appModulePath = "/root/app/infrastructure/app.nix";
in
  { config, pkgs, lib, ... } :
  (
    {
      fileSystems."/data" = {
        device = "/dev/sda";
        fsType = "ext4";
        autoFormat = true;
        autoResize = true;
      };

      swapDevices = [ { device = "/swapfile"; size = 4096; } ];

      system.autoUpgrade.enable = true;

    } // (if lib.pathExists appModulePath then {

      services.myapp.env = "${var.env}";
      # services.myapp.debug = true;

      imports = [ appModulePath ];

    } else
      lib.warn ("App module not found at " + appModulePath + ". Skipping App Configuration.") {}
    )
  )
EOF
}

resource "random_id" "django_secret_key" {
  byte_length = 32

  lifecycle {
    create_before_destroy = true
  }
}

data "template_file" "secrets" {
  template = <<EOF
# Provisioned by Terraform

DJANGO_SECRET_KEY='${random_id.django_secret_key.b64_url}'
DJANGO_SECRET_EXAMPLE='${var.django_secret_example}'
EOF
}

resource "digitalocean_droplet" "app" {
  name       = "myapp-${var.env}"
  size       = "s-2vcpu-4gb"
  image      = "debian-9-x64"
  region     = "${var.do_region}"
  ssh_keys   = ["${digitalocean_ssh_key.app.id}"]
  volume_ids = ["${digitalocean_volume.data.id}"]
  ipv6       = true

  private_networking = true

  lifecycle {
    create_before_destroy = true
  }

  connection {
    user        = "root"
    host        = "${digitalocean_droplet.app.ipv4_address}"
    private_key = "${tls_private_key.app.private_key_pem}"
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "file" {
    content     = "${data.template_file.secrets.rendered}"
    destination = "/root/SECRET_vars"
  }

  provisioner "remote-exec" {
    inline = [
      "echo -e 'silent\nshow-error\nretry=2' | tee ~/.curlrc > /dev/null",
      "wget 'https://raw.githubusercontent.com/elitak/nixos-infect/5d08c81d0915b6316c3e2a307a286e4e6d3d265d/nixos-infect' -O /root/nixos-infect",
      "echo 'Installing NixOS. Logging to /tmp/infect.log.'",
      "sed -i 's/reboot/shutdown -r -t 1/' /root/nixos-infect",
      "NIXOS_IMPORT=/root/host.nix NIX_CHANNEL=nixos-18.03 bash /root/nixos-infect 2>&1 > /tmp/infect.log",
    ]
  }

  provisioner "local-exec" {
      command = "sleep 70"
  }

  provisioner "remote-exec" {
    inline = [
      "nixos-version",
      "cp /old-root/root/host.nix /root/host.nix",
      "cp /old-root/root/.curlrc /root/.curlrc",
      "rm -rf /old-root",
    ]
  }
}

resource "digitalocean_floating_ip" "app" {
  droplet_id = "${digitalocean_droplet.app.id}"
  region     = "${digitalocean_droplet.app.region}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_record" "app" {
  domain     = "${var.domain}"
  type   = "A"
  ttl = 300
  name       = "app${var.env == "production" ? "" : ".${var.env}"}"
  value  = "${digitalocean_floating_ip.app.ip_address}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "null_resource" "deploy" {
  depends_on = [
    "digitalocean_volume.data",
    "digitalocean_record.app"
  ]

  triggers {
    droplet = "${digitalocean_droplet.app.id}"
    always  = "${uuid()}"
  }

  connection {
    user        = "root"
    host        = "${digitalocean_floating_ip.app.ip_address}"
    private_key = "${tls_private_key.app.private_key_pem}"
  }

  provisioner "remote-exec" {
    inline = ["echo 'Hello, World!'"]
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "file" {
    content     = "${data.template_file.secrets.rendered}"
    destination = "/root/SECRET_vars"
  }

  provisioner "local-exec" {
    command = "./deploy -i ${path.module}/SECRET_private_key root@${digitalocean_floating_ip.app.ip_address}"
  }
}

resource "local_file" "ip" {
  filename = "${path.module}/ip"
  content  = "${digitalocean_floating_ip.app.ip_address}"
}

resource "local_file" "private_key" {
  filename = "${path.module}/SECRET_private_key"
  content  = "${tls_private_key.app.private_key_pem}"

  provisioner "local-exec" {
    command = "chmod 600 ${path.module}/SECRET_private_key"
  }
}

output "ip" {
  value = "${digitalocean_floating_ip.app.ip_address}"
}

output "fqdn" {
  value = "${digitalocean_record.app.fqdn}"
}

output "private_key_file" {
  value = "${path.module}/SECRET_private_key"
}
