# nix-shell environment for provisioning infrastructure and deployment

{pkgs ? import ../nixpkgs-pinned.nix {}, ...} :
let
  terraform = pkgs.terraform_0_11.withPlugins (p : with p;
    [digitalocean local p.null random template tls ]
  );
in
  pkgs.stdenv.mkDerivation {
    name = "myapp-deploy-env";
    buildInputs = [ terraform ] ++ (with pkgs; [ git rsync openssh ]);
  }
