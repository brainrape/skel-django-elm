# nix-shell environment for frontend development

{ pkgs ? import ../nixpkgs-pinned.nix {}, ... }:
let
  frontendTools = import ./frontend-tools.nix { inherit pkgs; };
in pkgs.lib.overrideDerivation (import ./default.nix { inherit pkgs; }) (old: {
  name = "myapp-frontend-shell";
  buildInputs = old.buildInputs ++ ( with pkgs; [
    nodejs
    elmPackages.elm
    elmPackages.elm-format
    frontendTools.elm2nix
    frontendTools.updateElmDeps
    frontendTools.updateNpmDeps
    frontendTools.updateFrontendDeps
  ]);
  shellHook = old.shellHook + ''
    export DEBUG=true
    export SERVER_URL="http://localhost:8000/"
  '';
})
