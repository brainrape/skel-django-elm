module Tests.Login exposing (suite)

import Expect exposing (Expectation)
import Http exposing (..)
import Maybe.Extra exposing (isJust)
import Pages.Login
import Test exposing (..)


suite : Test
suite =
    let
        ctx =
            { baseUrl = "fake", session = Nothing }

        getModel =
            \( it, _, _ ) -> it

        ( initModel, _ ) =
            Pages.Login.init ctx

        validModel =
            { initModel | inputEmail = "a@a.aa", inputPassword = "a" }

        invalidEmailModel =
            { validModel | inputEmail = "aa" }

        invalidPasswordModel =
            { validModel | inputPassword = "" }

        thereAreErrors ( model, _, _ ) =
            Expect.true "There are errors" (isJust model.inputError)
    in
    describe "Login Tests"
        [ test "Valid" <|
            \_ ->
                Expect.equal
                    (Pages.Login.update ctx Pages.Login.ClickSubmit validModel |> getModel)
                    validModel
        , test "Invalid Email" <|
            \_ ->
                thereAreErrors <|
                    Pages.Login.update ctx Pages.Login.ClickSubmit invalidEmailModel
        , test "Invalid Password" <|
            \_ ->
                thereAreErrors <|
                    Pages.Login.update ctx Pages.Login.ClickSubmit invalidPasswordModel
        , test "Valid Response" <|
            \_ ->
                Expect.equal
                    (Pages.Login.update ctx
                        (Pages.Login.ResponseLogin "a@a.aa" (Ok (Ok ( "TEST_TOKEN", False ))))
                        validModel
                        |> getModel
                    )
                    validModel
        , test "Invalid Response" <|
            \_ ->
                thereAreErrors <|
                    Pages.Login.update ctx
                        (Pages.Login.ResponseLogin "a@a.aa" (Ok (Err [ "TEST_ERROR" ])))
                        validModel
        , test "Error Response" <|
            \_ ->
                thereAreErrors <|
                    Pages.Login.update ctx
                        (Pages.Login.ResponseLogin "a@a.aa" (Err NetworkError))
                        validModel
        ]
