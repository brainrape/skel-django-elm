module Tests.Item exposing (suite)

import Data exposing (..)
import Expect exposing (Expectation)
import Http exposing (..)
import Pages.Item
import RemoteData exposing (..)
import Rest exposing (..)
import Test exposing (..)


suite : Test
suite =
    let
        ctx =
            { baseUrl = "fake", session = Nothing }

        getModel =
            \( it, _, _ ) -> it

        ( initModel, _ ) =
            Pages.Item.init ctx 0

        item =
            { initItem | uid = "abc" }
    in
    describe "Item Tests"
        [ test "Valid Response" <|
            \_ ->
                Expect.equal
                    (initModel
                        |> Pages.Item.update ctx
                            (Pages.Item.ResponseGetItem (Ok item))
                        |> getModel
                        |> .item
                    )
                    (Success item)
        , test "Error Response" <|
            \_ ->
                Expect.equal
                    (initModel
                        |> Pages.Item.update ctx
                            (Pages.Item.ResponseGetItem (Err Http.NetworkError))
                        |> getModel
                        |> .item
                    )
                    (Failure ())
        ]
