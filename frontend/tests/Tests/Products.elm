module Tests.Products exposing (suite)

import Data exposing (..)
import Expect exposing (Expectation)
import Http exposing (..)
import Pages.Products
import RemoteData exposing (..)
import Rest exposing (..)
import Test exposing (..)


suite : Test
suite =
    let
        ctx =
            { baseUrl = "fake", session = Nothing }

        getModel =
            \( it, _, _ ) -> it

        ( initModel, _ ) =
            Pages.Products.init ctx

        product =
            ProductBrief 1 "Test" Nothing
    in
    describe "Products Tests"
        [ test "Valid Response" <|
            \_ ->
                Expect.equal
                    (initModel
                        |> Pages.Products.update ctx
                            (Pages.Products.ResponseGetProducts (Ok [ product ]))
                        |> getModel
                    )
                    (Success [ product ])
        , test "Error Response" <|
            \_ ->
                Expect.equal
                    (initModel
                        |> Pages.Products.update ctx
                            (Pages.Products.ResponseGetProducts (Err Http.NetworkError))
                        |> getModel
                    )
                    (Failure Http.NetworkError)
        ]
