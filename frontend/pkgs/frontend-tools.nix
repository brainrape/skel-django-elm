# Tools for building an Elm-based frontend

{ pkgs ? import ../../nixpkgs-pinned.nix {}, ...} :
let
  nodejs-major-version = "10";
  nodejs-attr = "nodejs-${nodejs-major-version}_x";
  elm2nix = pkgs.writeScriptBin "elm2nix" ''
    #!${pkgs.ruby}/bin/ruby
    require 'json'

    depsSrc = JSON.parse(File.read("elm.json"))
    depList = depsSrc['dependencies']['direct'].merge(depsSrc['dependencies']['indirect'])
    deps = Hash[ depList.map { |pkg, ver|
      url = "https://github.com/#{pkg}/archive/#{ver}.tar.gz"
      sha256 = `${pkgs.nix}/bin/nix-prefetch-url #{url}`

      [ pkg, { version: ver,
               sha256: sha256.strip
             }
      ]
    } ]

    File.open("pkgs/elm-packages.nix", 'w') do |file|
      file.puts "{"
        for pkg, info in deps
          file.puts "  \"#{pkg}\" = {"
          file.puts "    version = \"#{info[:version]}\";"
          file.puts "    sha256 = \"#{info[:sha256]}\";"
          file.puts "  };"
        end
      file.puts "}"
    end
  '';

  updateNpmDeps = pkgs.writeShellScriptBin "update-npm-deps" ''
    set -e
    echo; echo "Pinning node dependencies from ./package.json ..."; echo
    ${pkgs.nix}/bin/nix-instantiate --strict --json --eval --expr '
      (import <nixpkgs> {}).lib.mapAttrsToList
        (name : value : { "${"\${name}"}" = value; })
        (builtins.fromJSON (builtins.readFile ./package.json)).dependencies
    ' > pkgs/node-packages.json
    cd pkgs
    ${pkgs.nodePackages.node2nix}/bin/node2nix \
      --input node-packages.json \
      --flatten \
      --strip-optional-dependencies \
      --pkg-name ${nodejs-attr} \
      ${if (nodejs-major-version == "10") then "--nodejs-10" else "-${nodejs-major-version}" }

    ${pkgs.gnused}/bin/sed -i -e "s| name = \"elm-webpack-loader\";| dontNpmInstall = true;\n    name = \"elm-webpack-loader\";|" node-packages.nix
    ${pkgs.gnused}/bin/sed -i -e "s| sources.\"elm-0\.1| # sources.\"elm-0.1|" node-packages.nix
  '';

  updateElmDeps = pkgs.writeShellScriptBin "update-elm-deps" ''
    set -e
    #TODO echo; echo "Updating tests/elm.json..."
    #TODO ./node_modules/elm-test/bin/elm-test --add-dependencies tests/elm.json

    echo; echo "Pinning elm dependencies from ./elm.json"; echo
    rm -rf elm-stuff || true
    rm -rf .elm || true
    rm -rf tests/elm-stuff || true
    ln -s ../elm-stuff tests/elm-stuff
    export ELM_PATH=$PWD/.elm
    ${pkgs.elmPackages.elm}/bin/elm make src/Main.elm --output=/dev/null
    cp .elm/0.19.0/package/versions.dat pkgs/versions.dat
    ${elm2nix}/bin/elm2nix
  '';

  updateFrontendDeps = pkgs.writeShellScriptBin "update-frontend-deps" ''
    set -e
    ${updateNpmDeps}/bin/update-npm-deps
    ${updateElmDeps}/bin/update-elm-deps
  '';

  makeElmStuff = deps:
    let
      inherit (pkgs) lib fetchurl;
      json = builtins.toJSON (lib.mapAttrs (name: info: info.version) deps);
      cmds = lib.mapAttrsToList (name: info: let
        pkg = pkgs.stdenv.mkDerivation {
          name = lib.replaceStrings ["/"] ["-"] name + "-${info.version}";
          src = fetchurl {
            url = "https://github.com/${name}/archive/${info.version}.tar.gz";
            meta.homepage = "https://github.com/${name}/";
            inherit (info) sha256;
          };
          phases = [ "unpackPhase" "installPhase" ];
          installPhase = ''
            mkdir -p $out
            cp -r * $out
            '';
          };
        in ''
          mkdir -p elm-stuff/packages/${name}
          ln -s ${pkg} elm-stuff/packages/${name}/${info.version}
        '') deps;
    in ''
      home_old=$HOME
      HOME=/tmp
      mkdir elm-stuff
      cat > elm-stuff/exact-dependencies.json <<EOF
      ${json}
      EOF
      ${lib.concatStrings cmds}
      HOME=$home_old
    '';

  mkFrontend =
    allArgs@
    { src
    , node_modules
    , elm_packages
    , shellHook ? ""
    , ...
    }:
    let
      args = removeAttrs allArgs ["src" "nodejs" "node_modules" "elm_packages"];
      self = pkgs.stdenv.mkDerivation ({
        src =
          builtins.filterSource (path: type:
            baseNameOf path != ".elm" && baseNameOf path != "elm-stuff" && baseNameOf path != "node_modules"
          ) src;

        buildInputs = [ pkgs."${nodejs-attr}" pkgs.elmPackages.elm ] ++ (builtins.attrValues node_modules);

        configurePhase = ''
          set -e
          rm -rf node_modules || true
          mkdir node_modules
          for item in ${builtins.concatStringsSep " " (builtins.attrValues node_modules)}; do
            ln -s $item/lib/node_modules/* ./node_modules
          done

          export NODE_PATH=$PWD/node_modules:$NODE_PATH

          rm -rf elm-stuff || true
          rm -rf .elm || true
          export ELM_PATH=$PWD/.elm
        '' + pkgs.elmPackages.fetchElmDeps {
          elmPackages = (import ./elm-packages.nix);
          versionsDat = ./versions.dat;
        };
        #+ makeElmStuff elm_packages;

        buildPhase = ''
          webpack --progress --display-errors-details
        '';

        doCheck = true;

        checkPhase = ''
          echo; echo "Running ... elm-format-0.18 src/ --validate"
          elm-format src/ --validate
          echo; echo "Running ... elm-format-0.18 tests/ --validate"
          elm-format tests/ --validate
          #TODO echo; echo "Running ... elm-test"
          #TODO ./node_modules/elm-test/bin/elm-test
        '';

        installPhase = ''
          mkdir -p $out/public
          cp -R dist/* $out/public/
          runHook postInstall
        '';

        shellHook = ''
          set -e
          ${self.configurePhase}
          ${shellHook}
          set +e
        '';
      } // args);
    in self;
in
  {
    inherit elm2nix;
    inherit mkFrontend;
    inherit updateElmDeps;
    inherit updateNpmDeps;
    inherit updateFrontendDeps;
  }
