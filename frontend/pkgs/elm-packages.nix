{
  "NoRedInk/elm-json-decode-pipeline" = {
    version = "1.0.0";
    sha256 = "0y25xn0yx1q2xlg1yx1i0hg4xq1yxx6yfa99g272z8162si75hnl";
  };
  "elm/browser" = {
    version = "1.0.1";
    sha256 = "1zlmx672glg7fdgkvh5jm47y85pv7pdfr5mkhg6x7ar6k000vyka";
  };
  "elm/core" = {
    version = "1.0.2";
    sha256 = "1l0qdbczw91kzz8sx5d5zwz9x662bspy7p21dsr3f2rigxiix2as";
  };
  "elm/html" = {
    version = "1.0.0";
    sha256 = "1n3gpzmpqqdsldys4ipgyl1zacn0kbpc3g4v3hdpiyfjlgh8bf3k";
  };
  "elm/http" = {
    version = "1.0.0";
    sha256 = "1igmm89ialzrjib1j8xagkxalq1x2gj4l0hfxcd66mpwmvg7psl8";
  };
  "elm/json" = {
    version = "1.1.2";
    sha256 = "1a107nmm905dih4w4mjjkkpdcjbgaf5qjvr7fl30kkpkckfjjnrw";
  };
  "elm/time" = {
    version = "1.0.0";
    sha256 = "0vch7i86vn0x8b850w1p69vplll1bnbkp8s383z7pinyg94cm2z1";
  };
  "elm/url" = {
    version = "1.0.0";
    sha256 = "0av8x5syid40sgpl5vd7pry2rq0q4pga28b4yykn9gd9v12rs3l4";
  };
  "elm-community/list-extra" = {
    version = "8.1.0";
    sha256 = "0ngynq305b45zkz0398z66ldb1xl3jjnzdrj8fddim1zxjv2jdiw";
  };
  "elm-community/maybe-extra" = {
    version = "5.0.0";
    sha256 = "10jykkdmqpvhbcm50867a894272j4z6jc9r5jy35cz8jfi9j8xv9";
  };
  "hecrj/html-parser" = {
    version = "2.0.0";
    sha256 = "0pla6hswsl9piwrj3yl4pc4nfs5adc4g4c93644j4xila7bqqg8a";
  };
  "krisajenkins/remotedata" = {
    version = "5.0.0";
    sha256 = "0sjrgpsy4aci0h6qndqx70cnfd065hlkh1rxk0lc93z22sdj4knl";
  };
  "lukewestby/elm-http-builder" = {
    version = "6.0.0";
    sha256 = "1rby7lfsaj9iv9dlvy0h4arixg5y04p0jbn5hrpi62bj3qf7ap7f";
  };
  "rtfeldman/elm-validate" = {
    version = "4.0.1";
    sha256 = "116svp0b9n6nxcp65s89yiil50cfxxrcs2fi19sa6aw8k19i4h4m";
  };
  "rundis/elm-bootstrap" = {
    version = "5.0.0";
    sha256 = "0wz9di43l545jkq3g5zmfs0yr9yk24cy764mrd1mlnh9zqiv3gzq";
  };
  "avh4/elm-color" = {
    version = "1.0.0";
    sha256 = "0n16wnvp87x9az3m5qjrl6smsg7051m719xn5d244painx8xmpzq";
  };
  "elm/parser" = {
    version = "1.1.0";
    sha256 = "0a3cxrvbm7mwg9ykynhp7vjid58zsw03r63qxipxp3z09qks7512";
  };
  "elm/regex" = {
    version = "1.0.0";
    sha256 = "0lijsp50w7n1n57mjg6clpn9phly8vvs07h0qh2rqcs0f1jqvsa2";
  };
  "elm/virtual-dom" = {
    version = "1.0.2";
    sha256 = "0q1v5gi4g336bzz1lgwpn5b1639lrn63d8y6k6pimcyismp2i1yg";
  };
  "rtfeldman/elm-hex" = {
    version = "1.0.0";
    sha256 = "1y0aa16asvwdqmgbskh5iba6psp43lkcjjw9mgzj3gsrg33lp00d";
  };
}
