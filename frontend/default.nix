# Nix expression defining the frontend derivation

{ pkgs ? import ../nixpkgs-pinned.nix {}, debug ? false, serverUrl ? "http://localhost:3000/", ...} :
let
  frontendTools = import ./frontend-tools.nix { inherit pkgs; };
  buildCommand = "webpack --progress --display-errors-details";
in frontendTools.mkFrontend rec {
  name = "myapp-frontend";
  src = ./.;
  node_modules = import ./pkgs/default.nix { inherit pkgs; };
  elm_packages = import ./pkgs/elm-packages.nix;
  buildPhase = if debug then ''
      DEBUG=true SERVER_URL=${serverUrl} ${buildCommand}
    '' else ''
      DEBUG= SERVER_URL=${serverUrl} ${buildCommand}
    '';
}
