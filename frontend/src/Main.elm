module Main exposing (main)

import App exposing (Model, Msg(..), initCfg, subscriptions, update)
import Browser
import Routes exposing (parseRoute)
import Views.App exposing (view)


main : Program { baseUrl : String, cookie : String } Model Msg
main =
    Browser.application
        { init = initCfg
        , view = \model -> { title = "myapp", body = [ view model ] }
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = OnUrlRequest
        , onUrlChange = OnUrlChange
        }
