module Validated exposing (Validated(..), invalidErrors, invalidate, validated, validatedEmail, validatedPassword, validatedValue)

import Validate exposing (..)


type Validated a
    = Initial
    | Valid a
    | Invalid a (List String)


validated : Validator String a -> a -> Validated a
validated validator input =
    case validate validator input of
        Ok v ->
            Valid input

        Err xs ->
            Invalid input xs


invalidate : Validated String -> List String -> Validated String
invalidate v errs =
    case v of
        Valid a ->
            Invalid a errs

        Invalid a _ ->
            Invalid a errs

        Initial ->
            Invalid "" errs


validatedValue : Validated String -> String
validatedValue v =
    case v of
        Valid str ->
            str

        Invalid str _ ->
            str

        Initial ->
            ""


invalidErrors : Validated a -> List String
invalidErrors v =
    case v of
        Invalid _ errs ->
            errs

        _ ->
            []


validatedEmail : String -> Validated String
validatedEmail str =
    validated (ifInvalidEmail identity (\_ -> "Please enter a valid email address")) str


validatedPassword : String -> Validated String
validatedPassword str =
    if String.length str > 0 then
        Valid str

    else
        Invalid str [ "Please enter your password" ]
