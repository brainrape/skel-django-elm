module Views.App exposing (view)

import App exposing (Model, Msg(..), Page(..))
import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.CDN as CDN
import Bootstrap.Grid as Grid
import Bootstrap.Navbar as Navbar
import Context exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Views.Home
import Views.Item
import Views.Login


view : Model -> Html Msg
view model =
    div [ id "scrollTop" ]
        [ viewNavbar model.ctx model.navbarState
        , br [] []
        , Grid.container []
            [ viewPage model.ctx model.page
            , model.error |> Maybe.map viewError |> Maybe.withDefault (text "")
            ]
        , br [] []
        , br [] []
        , br [] []
        , br [] []
        , br [] []
        ]


viewNavbar : Ctx -> Navbar.State -> Html Msg
viewNavbar ctx navbarState =
    Navbar.config NavbarMsg
        |> Navbar.attrs [ class "navbar-expand" ]
        |> Navbar.container
        |> Navbar.brand [ href "/" ] [ text "myapp" ]
        |> (ctx.session |> Maybe.map navbarItems |> Maybe.withDefault identity)
        |> Navbar.view navbarState


navbarItems { email } =
    Navbar.items
        [ Navbar.dropdown
            { id = "nav"
            , toggle = Navbar.dropdownToggle [ href "/NONE" ] [ text email ]
            , items =
                [ Navbar.dropdownItem [ href "/" ] [ text "Home" ]
                , Navbar.dropdownDivider
                , Navbar.dropdownItem [ href "/items/" ] [ text "All Items" ]
                , Navbar.dropdownItem [ href "/items/new" ] [ text "New Item" ]
                , Navbar.dropdownItem [ href "/items/test" ] [ text "Test Item" ]
                , Navbar.dropdownDivider
                , Navbar.dropdownItem [ href "/login", onClick LogOut ] [ text "Log Out" ]
                ]
            }
        ]


viewPage : Ctx -> Page -> Html Msg
viewPage ctx page =
    case page of
        Blank ->
            pre [] [ text "..." ]

        LoginPage model ->
            Html.map LoginMsg <| Views.Login.view model

        ItemPage model ->
            Html.map ItemMsg <| Views.Item.view ctx model

        HomePage model ->
            Html.map HomeMsg <| Views.Home.view ctx model


viewError : String -> Html Msg
viewError err =
    div
        [ style "top" "6px"
        , style "left" "0"
        , style "right" "0"
        , style "position" "fixed"
        , style "'z-index" "10"
        , style "text-align" "center"
        ]
        [ div []
            [ Alert.config
                |> Alert.danger
                |> Alert.children [ text err ]
                |> Alert.dismissable (always ClearError)
                |> Alert.view Alert.shown
            ]
        ]
