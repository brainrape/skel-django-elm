module Views.Errors exposing (viewInputError, viewNetworkError, viewRetry)

import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Utilities.Spacing as Spacing
import Data exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Views.Styles exposing (..)


viewNetworkError : Html msg
viewNetworkError =
    text "Oops. Something went wrong. Please reload the page and try again."


viewRetry : msg -> Html msg
viewRetry msg =
    div []
        [ div [] [ text "Hmm, something's wrong. I couldn't load the data from the server." ]
        , div [ Spacing.mt3 ] []
        , Button.button [ Button.onClick msg ] [ text "Retry" ]
        ]


viewInputError : ErrorStrings -> Html msg
viewInputError errs =
    if errs == [] then
        Alert.simpleDanger [ style "visibility" "hidden" ] [ text "" ]

    else
        Alert.simpleDanger [] (errs |> List.map (\e -> div [] [ text e ]))
