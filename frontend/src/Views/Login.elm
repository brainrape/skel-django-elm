module Views.Login exposing (view)

import Bootstrap.Button as Button
import Bootstrap.Form as Form
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Html exposing (..)
import Html.Attributes exposing (..)
import Pages.Login exposing (..)
import Validated exposing (..)
import Views.Input exposing (..)


view : Model -> Html Msg
view model =
    Grid.row
        [ Row.topXs ]
        [ Grid.col
            [ Col.sm2, Col.md3, Col.lg4 ]
            []
        , Grid.col
            [ Col.xs12, Col.sm8, Col.md6, Col.lg4 ]
            [ br [] []
            , h3 [] [ text "Log In" ]
            , form model
            ]
        ]


form : Model -> Html Msg
form model =
    Form.form []
        [ viewValidatedEmail ChangeEmail "email" "Email Address" (Valid model.inputEmail)
        , viewValidatedPassword ChangePassword "password" "Password" (Valid model.inputPassword)
        , div [ class "text-danger" ]
            [ p []
                (Maybe.withDefault [ "" ] model.inputError
                    |> List.map (\t -> [ text t, br [] [] ])
                    |> List.concat
                )
            ]
        , Button.button [ Button.primary, Button.onClick ClickSubmit ] [ text "Log In" ]
        ]
