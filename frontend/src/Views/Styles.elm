module Views.Styles exposing (stickyStyles, styles)

import Html exposing (Attribute)
import Html.Attributes exposing (style)


styles : List ( String, String ) -> List (Attribute msg)
styles pairs =
    pairs |> List.map (\( k, v ) -> style k v)


stickyStyles : List (Attribute msg)
stickyStyles =
    styles
        [ ( "position", "sticky" )
        , ( "padding-top", "6px" )
        , ( "padding-bottom", "6px" )
        , ( "top", "0px" )
        , ( "background", "white" )
        , ( "z-index", "1" )
        , ( "box-shadow", "0 8px 4px -4px #ddd" )
        ]
