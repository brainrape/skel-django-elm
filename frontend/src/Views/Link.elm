module Views.Link exposing (viewLink)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode exposing (succeed)
import Routes exposing (..)


viewLink : Route -> msg -> String -> Html msg
viewLink route msg str =
    a
        [ href (reverseRoute route)
        , onWithOptions "click" { defaultOptions | preventDefault = True } (succeed msg)
        ]
        [ text str ]
