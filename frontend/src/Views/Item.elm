module Views.Item exposing (view)

import Bootstrap.Button as Button
import Bootstrap.Form.Checkbox as Checkbox
import Bootstrap.Form.Input as Input
import Bootstrap.Form.Textarea as Textarea
import Bootstrap.Utilities.Spacing as Spacing
import Context exposing (Ctx)
import Data exposing (fi, initItem)
import Html exposing (..)
import Html.Attributes exposing (..)
import Pages.Item exposing (..)
import RemoteData exposing (..)
import Views.Errors exposing (..)
import Views.Form exposing (..)
import Views.Input exposing (..)
import Views.Styles exposing (stickyStyles, styles)


view : Ctx -> Model -> Html Msg
view ctx model =
    case model.item of
        Failure _ ->
            viewRetry ClickRetry

        Loading ->
            text "Loading"

        _ ->
            div []
                [ div ([ class "clearfix" ] ++ stickyStyles)
                    [ h4 []
                        [ case model.uid of
                            Nothing ->
                                text "New Item"

                            Just uid ->
                                text <| "Item " ++ uid
                        , span [ class "float-right" ] [ viewSaveButton model ]
                        ]
                    ]
                , div
                    [ style "padding" "0 8px" ]
                    [ viewInputError model.inputError
                    , div [ Spacing.mt2 ]
                        [ h5 [] [ text "Text" ]
                        , div [ Spacing.mt3 ] []
                        , viewInput Input.text ChangeText "text" "" model.input.text []
                        ]
                    , div [ Spacing.mt5 ]
                        [ hr [] []
                        , viewSaveButton model
                        ]
                    ]
                ]


viewSaveButton : Model -> Html Msg
viewSaveButton model =
    let
        btn canDiscard =
            span [ class "float-right" ]
                [ if canDiscard then
                    Button.button [ Button.onClick ClickDiscard ] [ text "Discard Changes" ]

                  else
                    text ""
                , text " "
                , Button.button [ Button.onClick ClickSave, Button.primary ] [ text "Save" ]
                ]
    in
    case model.item of
        Success item ->
            if item /= model.input then
                btn True

            else
                text ""

        NotAsked ->
            btn False

        _ ->
            text ""
