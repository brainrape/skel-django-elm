module Views.Home exposing (view)

import Bootstrap.Button as Button
import Bootstrap.Utilities.Spacing as Spacing
import Context exposing (..)
import Data exposing (fi)
import Html exposing (..)
import Html.Attributes exposing (..)
import Pages.Home exposing (..)
import RemoteData exposing (..)
import Views.Errors exposing (..)
import Views.Form exposing (..)
import Views.Input exposing (..)
import Views.Styles exposing (stickyStyles, styles)


view : Ctx -> Model -> Html Msg
view ctx model =
    div []
        [ h1 [] [ text "Home" ]
        , case model.home of
            Success home ->
                text home.text

            Loading ->
                text "Loading..."

            _ ->
                viewRetry ClickRetry
        ]
