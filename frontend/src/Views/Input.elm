module Views.Input exposing (validatedStatus, viewInput, viewValidated, viewValidatedEmail, viewValidatedPassword)

import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Form.Textarea as Textarea
import Bootstrap.Utilities.Spacing as Spacing
import Context exposing (Ctx)
import Data exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode
import Validated exposing (..)
import Views.Styles exposing (styles)


validatedStatus : Validated String -> List (Input.Option msg)
validatedStatus v =
    case v of
        Invalid _ _ ->
            [ Input.danger ]

        Valid _ ->
            []

        Initial ->
            []


viewValidated : (List (Input.Option msg) -> Html msg) -> (String -> msg) -> String -> String -> Validated String -> Html msg
viewValidated elem msg id title validated =
    Form.group []
        ([ Form.label [ for id ] [ text title ]
         , elem ([ Input.id id, Input.onInput msg, Input.value (validatedValue validated) ] ++ validatedStatus validated)
         ]
            ++ (validated |> invalidErrors |> List.map (\e -> Form.invalidFeedback [] [ text e ]))
        )


viewValidatedEmail : (String -> msg) -> String -> String -> Validated String -> Html msg
viewValidatedEmail =
    viewValidated Input.email


viewValidatedPassword : (String -> msg) -> String -> String -> Validated String -> Html msg
viewValidatedPassword =
    viewValidated Input.password


viewInput : (List (Input.Option msg) -> Html msg) -> (String -> msg) -> String -> String -> String -> List String -> Html msg
viewInput f msg id txt val errs =
    Form.group []
        ((if txt /= "" then
            [ Form.label [ for id ] [ text txt ] ]

          else
            []
         )
            ++ [ f
                    ([ Input.id id
                     , Input.onInput msg
                     , Input.value val
                     , Input.attrs [ attribute "autocomplete" "new-password" ]
                     ]
                        ++ (if List.isEmpty errs then
                                []

                            else
                                [ Input.danger ]
                           )
                    )
               ]
            ++ (if List.isEmpty errs then
                    [ Form.help (styles [ ( "visibility", "hidden" ) ]) [ text "." ] ]

                else
                    []
               )
            ++ (errs |> List.map (\e -> Form.invalidFeedback [] [ text e ]))
        )
