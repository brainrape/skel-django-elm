module Views.Form exposing (viewFormErrors, viewFormFooter)

import Bootstrap.Button as Button
import Data exposing (ErrorStrings)
import Html exposing (..)
import Html.Attributes exposing (..)
import RemoteData exposing (RemoteData(..))
import Rest exposing (networkErrorString)
import Views.Styles exposing (styles)


viewFormFooter : msg -> String -> RemoteData a (Result b c) -> ErrorStrings -> String -> Html msg
viewFormFooter msg txt response errs successTxt =
    div []
        [ Button.button [ Button.onClick msg, Button.primary ] [ text txt ]
        , span [ style "margin-left" "10px" ] []
        , viewFormErrors response errs successTxt
        ]


viewFormErrors : RemoteData a (Result b c) -> ErrorStrings -> String -> Html msg
viewFormErrors response errs txt =
    span []
        [ case response of
            Success (Ok _) ->
                small [ class "text-success" ] [ text txt ]

            Success (Err _) ->
                small [ class "text-danger" ] (errs |> List.map text |> List.intersperse (text " "))

            Failure _ ->
                small [ class "text-danger" ] [ text networkErrorString ]

            _ ->
                text ""
        ]
