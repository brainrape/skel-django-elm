module Data exposing (ErrorStrings, Home, Item, Session, Uid, decodeSession, encodeSession, fi, initHome, initItem, jsonDecHome, jsonDecItem, jsonDecNonFieldErrors, jsonDecSession, jsonDecStringListField, jsonEncItem, jsonEncSession, maybeEncode)

import Json.Decode as D exposing (Decoder)
import Json.Decode.Pipeline exposing (hardcoded, optional, required)
import Json.Encode as E exposing (Value)


type alias ErrorStrings =
    List String


type alias Uid =
    String


type alias Home =
    { text : String
    }


type alias Item =
    { uid : String
    , text : String
    }


type alias Session =
    { email : String
    , token : String
    }


fi : Bool -> a -> a -> a
fi if_ then_ else_ =
    if if_ then
        then_

    else
        else_


maybeEncode : (a -> Value) -> Maybe a -> Value
maybeEncode enc m =
    case m of
        Just x ->
            enc x

        _ ->
            E.null


jsonDecSession : Decoder Session
jsonDecSession =
    D.succeed Session
        |> required "email" D.string
        |> required "token" D.string


jsonEncSession : Session -> Value
jsonEncSession val =
    E.object
        [ ( "email", E.string val.email )
        , ( "token", E.string val.token )
        ]


decodeSession : String -> Maybe Session
decodeSession str =
    str
        |> D.decodeString jsonDecSession
        |> Result.toMaybe


encodeSession : Maybe Session -> String
encodeSession session =
    case session of
        Just session_ ->
            E.encode 0 (jsonEncSession session_)

        Nothing ->
            ""


jsonDecStringListField : String -> Decoder (List String)
jsonDecStringListField field =
    D.maybe (D.at [ field ] (D.list D.string))
        |> D.map (Maybe.withDefault [])


jsonDecNonFieldErrors : Decoder (List String)
jsonDecNonFieldErrors =
    D.oneOf
        [ D.at [ "nonFieldErrors" ] <| D.list <| D.string
        , D.list D.string
        , D.succeed []
        ]


initHome : Home
initHome =
    { text = ""
    }


jsonDecHome : Decoder Home
jsonDecHome =
    D.succeed Home
        |> required "text" D.string


initItem : Item
initItem =
    { uid = ""
    , text = ""
    }


jsonDecItem : Decoder Item
jsonDecItem =
    D.succeed Item
        |> required "uid" D.string
        |> required "text" D.string


jsonEncItem : Item -> Value
jsonEncItem val =
    E.object
        [ ( "uid", E.string val.uid )
        , ( "text", E.string val.text )
        ]
