module Rest exposing (ApiResult, Url, WebResult, delete, deleteSure, errorOfApiResult, get, getString, getSure, handleBadRequest, isUnauthenticated, maybeWithAuthHeader, networkErrorString, patch, patchForString, patchSure, post, postForString, postSure, put, putSure, request, requestSure, stringOfHttpError)

import Context exposing (Ctx)
import Http
import HttpBuilder exposing (..)
import Json.Decode
import Json.Encode


type alias WebResult a =
    Result Http.Error a


type alias ApiResult e a =
    Result Http.Error (Result e a)


type alias Url =
    String


maybeWithAuthHeader : Ctx -> (RequestBuilder a -> RequestBuilder a)
maybeWithAuthHeader ctx =
    ctx.session
        |> Maybe.map (.token >> withBearerToken)
        |> Maybe.withDefault identity


request :
    (String -> RequestBuilder ())
    -> Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder a
    -> Json.Decode.Decoder e
    -> (ApiResult e a -> msg)
    -> Cmd msg
request builderMethod ctx url data decoder decoderBadRequest handler =
    builderMethod (ctx.baseUrl ++ url)
        |> withJsonBody data
        |> maybeWithAuthHeader ctx
        |> withExpect (Http.expectJson decoder)
        |> send (handleBadRequest decoderBadRequest >> handler)


requestSure :
    (String -> RequestBuilder ())
    -> Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder a
    -> (WebResult a -> msg)
    -> Cmd msg
requestSure builderMethod ctx url data decoder handler =
    builderMethod (ctx.baseUrl ++ url)
        |> withJsonBody data
        |> maybeWithAuthHeader ctx
        |> withExpect (Http.expectJson decoder)
        |> send handler


get :
    Ctx
    -> Url
    -> Json.Decode.Decoder a
    -> Json.Decode.Decoder e
    -> (ApiResult e a -> msg)
    -> Cmd msg
get ctx url decoder decoderBadRequest handler =
    HttpBuilder.get (ctx.baseUrl ++ url)
        |> maybeWithAuthHeader ctx
        |> withExpect (Http.expectJson decoder)
        |> send (handleBadRequest decoderBadRequest >> handler)


getSure :
    Ctx
    -> Url
    -> Json.Decode.Decoder a
    -> (WebResult a -> msg)
    -> Cmd msg
getSure ctx url decoder handler =
    HttpBuilder.get (ctx.baseUrl ++ url)
        |> maybeWithAuthHeader ctx
        |> withExpect (Http.expectJson decoder)
        |> send handler


getString :
    Ctx
    -> Url
    -> (WebResult String -> msg)
    -> Cmd msg
getString ctx url handler =
    HttpBuilder.get (ctx.baseUrl ++ url)
        |> maybeWithAuthHeader ctx
        |> withExpect Http.expectString
        |> send handler


post :
    Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder a
    -> Json.Decode.Decoder e
    -> (ApiResult e a -> msg)
    -> Cmd msg
post =
    request HttpBuilder.post


postSure :
    Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder a
    -> (WebResult a -> msg)
    -> Cmd msg
postSure =
    requestSure HttpBuilder.post


postForString :
    Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder e
    -> (ApiResult e String -> msg)
    -> Cmd msg
postForString ctx url data decoderBadRequest handler =
    HttpBuilder.post (ctx.baseUrl ++ url)
        |> withJsonBody data
        |> maybeWithAuthHeader ctx
        |> withExpect Http.expectString
        |> send (handleBadRequest decoderBadRequest >> handler)


put :
    Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder a
    -> Json.Decode.Decoder e
    -> (ApiResult e a -> msg)
    -> Cmd msg
put =
    request HttpBuilder.put


putSure :
    Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder a
    -> (WebResult a -> msg)
    -> Cmd msg
putSure =
    requestSure HttpBuilder.put


patch :
    Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder a
    -> Json.Decode.Decoder e
    -> (ApiResult e a -> msg)
    -> Cmd msg
patch =
    request HttpBuilder.patch


patchSure :
    Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder a
    -> (WebResult a -> msg)
    -> Cmd msg
patchSure =
    requestSure HttpBuilder.patch


patchForString :
    Ctx
    -> Url
    -> Json.Encode.Value
    -> Json.Decode.Decoder e
    -> (ApiResult e String -> msg)
    -> Cmd msg
patchForString ctx url data decoderBadRequest handler =
    HttpBuilder.patch (ctx.baseUrl ++ url)
        |> withJsonBody data
        |> maybeWithAuthHeader ctx
        |> withExpect Http.expectString
        |> send (handleBadRequest decoderBadRequest >> handler)


delete :
    Ctx
    -> Url
    -> Json.Decode.Decoder e
    -> (ApiResult e () -> msg)
    -> Cmd msg
delete ctx url decoderBadRequest handler =
    HttpBuilder.delete (ctx.baseUrl ++ url)
        |> maybeWithAuthHeader ctx
        |> send (handleBadRequest decoderBadRequest >> handler)


deleteSure :
    Ctx
    -> Url
    -> (WebResult () -> msg)
    -> Cmd msg
deleteSure ctx url handler =
    HttpBuilder.delete (ctx.baseUrl ++ url)
        |> maybeWithAuthHeader ctx
        |> send handler


handleBadRequest :
    Json.Decode.Decoder e
    -> Result Http.Error a
    -> ApiResult e a
handleBadRequest decoder httpResult =
    case httpResult of
        Ok v ->
            Ok (Ok v)

        Err ((Http.BadStatus response) as err) ->
            case response.status.code of
                400 ->
                    case Json.Decode.decodeString decoder response.body of
                        Ok responseBadRequest ->
                            Ok (Err responseBadRequest)

                        Err e ->
                            Err (Http.BadPayload (Json.Decode.errorToString e) response)

                _ ->
                    Err err

        Err err ->
            Err err


stringOfHttpError : Http.Error -> String
stringOfHttpError err =
    case err of
        Http.BadUrl str ->
            "The server address is invalid. Please contact support."

        Http.Timeout ->
            "The server isn't responding. Please check the connection and try again."

        Http.NetworkError ->
            "It looks like we're having network issues. Please check the connection."

        Http.BadStatus response ->
            case response.status.code of
                400 ->
                    "The request was invalid. Please correct the input and try again."

                401 ->
                    "Not authenticated. Please log in."

                404 ->
                    "The server couldn't find the requested data."

                500 ->
                    "Oops. Something went wrong."

                _ ->
                    "We couldn't interpret the response from the server. Please reload the app and try again."

        Http.BadPayload str response ->
            "We couldn't interpret the response from the server. Please reload the app and try again."


errorOfApiResult : ApiResult e a -> Maybe Http.Error
errorOfApiResult result =
    case result of
        Err err ->
            Just err

        _ ->
            Nothing


isUnauthenticated : ApiResult e a -> Bool
isUnauthenticated result =
    case result of
        Err (Http.BadStatus response) ->
            response.status.code == 401

        _ ->
            False


networkErrorString : String
networkErrorString =
    "Hm, something's wrong. Please reload the page and try again."
