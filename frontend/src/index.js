'use strict';

require('./index.css')
require('./index.html')

function setCookie(cname, cvalue, exdays) {
    var d = new Date()
    d.setTime(d.getTime() + (exdays*24*60*60*1000))
    var expires = "expires="+ d.toUTCString()
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"
}

function getCookie(cname) {
    const name = cname + "="
    const decodedCookie = decodeURIComponent(document.cookie)
    const ca = decodedCookie.split(';')
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i]
        while (c.charAt(0) == ' ') {
            c = c.substring(1)
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length)
        }
    }
    return ""
}

import Elm from "./Main.elm"

const app = Elm.Elm.Main.init({
  flags :{
    baseUrl: process.env.SERVER_URL || "http://localhost:8000/",
    cookie : getCookie("session")
}})

app.ports.setCookie.subscribe(function(str) {
  setCookie("session", str, 30)
})
