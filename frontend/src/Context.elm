module Context exposing (Ctx)

import Browser.Navigation exposing (Key)
import Data exposing (Session)


type alias Ctx =
    { session : Maybe Session
    , baseUrl : String
    , key : Key
    }
