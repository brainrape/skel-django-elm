module Pages.Login exposing (Model, Msg(..), init, update)

import Context exposing (Ctx)
import Data exposing (ErrorStrings, Session, jsonDecNonFieldErrors, jsonDecSession)
import Json.Decode
import Json.Encode
import OutMsg exposing (..)
import Rest exposing (ApiResult, stringOfHttpError)
import Routes exposing (..)
import Validated exposing (..)


type alias Model =
    { inputEmail : String
    , inputPassword : String
    , inputError : Maybe (List String)
    }


type Msg
    = ChangeEmail String
    | ChangePassword String
    | ClickSubmit
    | ResponseLogin (ApiResult ErrorStrings Session)


init : Ctx -> ( Model, Cmd Msg )
init ctx =
    ( { inputEmail = ""
      , inputPassword = ""
      , inputError = Nothing
      }
    , Cmd.none
    )


update : Ctx -> Msg -> Model -> ( Model, Cmd Msg, OutMsg )
update ctx msg model =
    let
        updateModel model_ =
            ( model_, Cmd.none, OutNop )
    in
    case msg of
        ChangeEmail str ->
            updateModel { model | inputEmail = str, inputError = Nothing }

        ChangePassword str ->
            updateModel { model | inputPassword = str, inputError = Nothing }

        ClickSubmit ->
            case ( validatedEmail model.inputEmail, validatedPassword model.inputPassword ) of
                ( Valid email, Valid password ) ->
                    ( model, postLogin ctx email password, OutNop )

                ( email, password ) ->
                    updateModel { model | inputError = Just <| invalidErrors email ++ invalidErrors password }

        ResponseLogin result ->
            case result of
                Ok (Ok { email, token }) ->
                    ( model, Cmd.none, OutLogin { email = email, token = token } )

                Ok (Err err) ->
                    ( { model | inputError = Just err }, Cmd.none, OutNop )

                Err err ->
                    ( { model | inputError = Just [ Rest.networkErrorString ] }, Cmd.none, OutNop )


postLogin : Ctx -> String -> String -> Cmd Msg
postLogin ctx email password =
    Rest.post { ctx | session = Nothing }
        "account/login"
        (Json.Encode.object
            [ ( "email", Json.Encode.string email )
            , ( "password", Json.Encode.string password )
            ]
        )
        jsonDecSession
        jsonDecNonFieldErrors
        ResponseLogin
