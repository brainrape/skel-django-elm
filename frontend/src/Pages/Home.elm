module Pages.Home exposing (Model, Msg(..), init, update)

import Browser.Dom
import Context exposing (Ctx)
import Data exposing (ErrorStrings, Home, jsonDecHome, jsonDecNonFieldErrors)
import Json.Decode
import Json.Encode
import OutMsg exposing (..)
import Ports exposing (..)
import RemoteData exposing (..)
import Rest exposing (ApiResult, WebResult, stringOfHttpError)
import Task


type alias Model =
    { home : RemoteData () Home
    }


type Msg
    = Nop
    | ResponseGetHome (WebResult Home)
    | ClickRetry


initModel : Model
initModel =
    { home = Loading
    }


init : Ctx -> ( Model, Cmd Msg )
init ctx =
    ( initModel
    , getHome ctx
    )


update : Ctx -> Msg -> Model -> ( Model, Cmd Msg, OutMsg )
update ctx msg model =
    let
        pure model_ =
            ( model_, Cmd.none, OutNop )
    in
    case msg of
        Nop ->
            pure model

        ClickRetry ->
            ( model, getHome ctx, OutNop )

        ResponseGetHome result ->
            case result of
                Ok home ->
                    ( { model | home = Success home }, Cmd.none, OutNop )

                Err err ->
                    ( { model | home = Failure () }, Cmd.none, OutHttpError err )


getHome : Ctx -> Cmd Msg
getHome ctx =
    Rest.getSure ctx
        ""
        jsonDecHome
        ResponseGetHome
