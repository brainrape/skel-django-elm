module Pages.Item exposing (Model, Msg(..), init, update)

import Browser.Dom
import Context exposing (Ctx)
import Data exposing (..)
import Json.Decode
import Json.Encode
import OutMsg exposing (..)
import Ports exposing (..)
import RemoteData exposing (..)
import Rest exposing (ApiResult, WebResult, stringOfHttpError)
import Routes exposing (..)
import Task


type alias Model =
    { uid : Maybe Uid
    , item : RemoteData () Item
    , input : Item
    , inputError : ErrorStrings
    }


type Msg
    = Nop
    | ResponseGetItem (WebResult Item)
    | ResponsePatchItem (ApiResult ErrorStrings Item)
    | ChangeText String
    | ClickSave
    | ClickDiscard
    | ClickRetry


initModel : Model
initModel =
    { uid = Nothing
    , item = NotAsked
    , input = Data.initItem
    , inputError = []
    }


init : Ctx -> Maybe Uid -> ( Model, Cmd Msg )
init ctx uid =
    case uid of
        Just uid_ ->
            ( { initModel | uid = uid, item = Loading }, getItem ctx uid_ )

        Nothing ->
            ( initModel, Cmd.none )


scrollToTop : Cmd Msg
scrollToTop =
    Browser.Dom.setViewport 0 0 |> Task.perform (\r -> Nop)


update : Ctx -> Msg -> Model -> ( Model, Cmd Msg, OutMsg )
update ctx msg model =
    let
        input =
            model.input

        pure model_ =
            ( model_, Cmd.none, OutNop )
    in
    case msg of
        ResponseGetItem result ->
            case result of
                Ok item ->
                    pure { model | item = Success item, input = item }

                Err err ->
                    ( { model | item = Failure () }, Cmd.none, OutHttpError err )

        ResponsePatchItem result ->
            case result of
                Ok (Ok item) ->
                    pure { model | uid = Just item.uid, item = Success item, inputError = [], input = item }

                Ok (Err err) ->
                    let
                        err_ =
                            if List.isEmpty err then
                                [ Rest.networkErrorString ]

                            else
                                err
                    in
                    ( { model | inputError = err_ }, scrollToTop, OutNop )

                Err err ->
                    ( { model | inputError = [ Rest.networkErrorString ] }, scrollToTop, OutHttpError err )

        ChangeText str ->
            pure { model | input = { input | text = str } }

        ClickRetry ->
            case model.uid of
                Just uid_ ->
                    ( model, getItem ctx uid_, OutNop )

                Nothing ->
                    pure model

        ClickSave ->
            ( model
            , fi (model.uid == Nothing) postItem patchItem ctx model.input
            , OutNop
            )

        ClickDiscard ->
            case model.item of
                Success item ->
                    ( { model | input = item }
                    , Cmd.none
                    , OutNop
                    )

                _ ->
                    pure model

        Nop ->
            pure model


getItem : Ctx -> String -> Cmd Msg
getItem ctx uid =
    Rest.getSure ctx
        ("items/" ++ uid)
        jsonDecItem
        ResponseGetItem


patchItem : Ctx -> Item -> Cmd Msg
patchItem ctx item =
    Rest.patch ctx
        ("items/" ++ item.uid)
        (jsonEncItem item)
        jsonDecItem
        jsonDecNonFieldErrors
        ResponsePatchItem


postItem : Ctx -> Item -> Cmd Msg
postItem ctx item =
    Rest.post ctx
        "items"
        (jsonEncItem item)
        jsonDecItem
        (Json.Decode.map2 (\a b -> a ++ b)
            (jsonDecStringListField "text"
                |> Json.Decode.map
                    (\l -> fi (l /= [ "This field may not be blank." ]) l [ "Please enter some text." ])
            )
            jsonDecNonFieldErrors
        )
        ResponsePatchItem
