module App exposing (Model, Msg(..), Page(..), init, initCfg, subscriptions, update)

import Bootstrap.Navbar as Navbar
import Browser
import Browser.Navigation exposing (Key)
import Context exposing (..)
import Data exposing (..)
import Http
import OutMsg exposing (..)
import Pages.Home
import Pages.Item
import Pages.Login
import Ports
import Rest
import Routes exposing (Route(..), goTo, parseRoute)
import Task
import Url exposing (Url)


type alias Model =
    { ctx : Ctx
    , page : Page
    , navbarState : Navbar.State
    , error : Maybe String
    }


type Page
    = Blank
    | LoginPage Pages.Login.Model
    | HomePage Pages.Home.Model
    | ItemPage Pages.Item.Model


type Msg
    = ChangeRoute Route
    | NavbarMsg Navbar.State
    | LoginMsg Pages.Login.Msg
    | HomeMsg Pages.Home.Msg
    | ItemMsg Pages.Item.Msg
    | LogIn Session
    | LogOut
    | OnUrlRequest Browser.UrlRequest
    | OnUrlChange Url
    | ClearError


do : Msg -> Cmd Msg
do msg =
    Task.perform identity (Task.succeed msg)


initCfg : { baseUrl : String, cookie : String } -> Url -> Key -> ( Model, Cmd Msg )
initCfg { baseUrl, cookie } url key =
    init
        { baseUrl = baseUrl
        , session = decodeSession cookie
        , key = key
        }
        (parseRoute url)


init : Ctx -> Route -> ( Model, Cmd Msg )
init ctx route =
    let
        ( page, cmd ) =
            updateRoute ctx route

        ( navbarState, navbarCmd ) =
            Navbar.initialState NavbarMsg
    in
    ( { ctx = ctx, page = page, navbarState = navbarState, error = Nothing }
    , Cmd.batch [ navbarCmd, cmd ]
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Navbar.subscriptions model.navbarState NavbarMsg
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        ctx =
            model.ctx
    in
    case msg of
        NavbarMsg navbarState ->
            ( { model | navbarState = navbarState }, Cmd.none )

        LogIn data ->
            ( { model | ctx = { ctx | session = Just data } }
            , Cmd.batch
                [ goTo ctx.key HomeRoute
                , Ports.setCookie (encodeSession (Just data))
                ]
            )

        LogOut ->
            init { ctx | session = Nothing } LoginRoute
                |> (\( m, cmd ) ->
                        ( m, Cmd.batch [ Ports.setCookie (encodeSession Nothing), cmd ] )
                   )

        OnUrlRequest (Browser.External url) ->
            ( model, Browser.Navigation.load url )

        OnUrlRequest (Browser.Internal url) ->
            if parseRoute url == NotFound then
                ( model, Cmd.none )

            else
                ( model, Browser.Navigation.pushUrl ctx.key (url |> Url.toString) )

        OnUrlChange url ->
            if parseRoute url == NotFound then
                ( model, Cmd.none )

            else
                let
                    ( page_, cmd_ ) =
                        updateRoute ctx (parseRoute url)
                in
                ( { model | page = page_ }, cmd_ )

        ClearError ->
            ( { model | error = Nothing }, Cmd.none )

        _ ->
            updatePage model.ctx msg model.page
                |> (\( page, cmd, outMsg ) ->
                        updateOut ctx outMsg { model | page = page }
                            |> (\( m, outCmd ) ->
                                    ( m, Cmd.batch [ cmd, outCmd ] )
                               )
                   )


updateOut : Ctx -> OutMsg -> Model -> ( Model, Cmd Msg )
updateOut ctx out model =
    case out of
        OutNop ->
            ( { model | error = Nothing }, Cmd.none )

        OutHttpError e ->
            case e of
                Http.BadStatus response ->
                    if response.status.code == 401 || response.status.code == 403 then
                        ( model, do LogOut )

                    else
                        ( model, Cmd.none )

                _ ->
                    ( { model | error = Just (Rest.stringOfHttpError e) }, Cmd.none )

        OutLogin data ->
            ( model, do (LogIn data) )

        OutLogOut ->
            ( model, do LogOut )


updatePage : Ctx -> Msg -> Page -> ( Page, Cmd Msg, OutMsg )
updatePage ctx msg page =
    let
        wrap wrapModel wrapMsg ( subModel, subCmd, outMsg ) =
            ( wrapModel subModel, Cmd.map wrapMsg subCmd, outMsg )
    in
    case ( msg, page ) of
        ( ChangeRoute route, _ ) ->
            updateRoute ctx route
                |> (\( page_, cmd ) ->
                        ( page_, cmd, OutNop )
                   )

        ( LoginMsg subMsg, LoginPage subModel ) ->
            Pages.Login.update ctx subMsg subModel
                |> wrap LoginPage LoginMsg

        ( HomeMsg subMsg, HomePage subModel ) ->
            Pages.Home.update ctx subMsg subModel
                |> wrap HomePage HomeMsg

        ( ItemMsg subMsg, ItemPage subModel ) ->
            Pages.Item.update ctx subMsg subModel
                |> wrap ItemPage ItemMsg

        _ ->
            ( page, Cmd.none, OutNop )


updateRoute : Ctx -> Route -> ( Page, Cmd Msg )
updateRoute ctx route =
    let
        wrap wrapModel wrapMsg ( subModel, subCmd ) =
            ( wrapModel subModel
            , Cmd.map wrapMsg subCmd
            )

        goHome =
            ( Blank, goTo ctx.key HomeRoute )
    in
    case ctx.session of
        Just _ ->
            case route of
                ItemRoute muid ->
                    Pages.Item.init ctx muid
                        |> wrap ItemPage ItemMsg

                HomeRoute ->
                    Pages.Home.init ctx
                        |> wrap HomePage HomeMsg

                LoginRoute ->
                    goHome

                NotFound ->
                    goHome

        Nothing ->
            case route of
                LoginRoute ->
                    Pages.Login.init ctx
                        |> wrap LoginPage LoginMsg

                _ ->
                    ( Blank, goTo ctx.key LoginRoute )
