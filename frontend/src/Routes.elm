module Routes exposing (Route(..), goTo, parseRoute, reverseRoute)

import Browser.Navigation exposing (Key)
import Url exposing (Url)
import Url.Parser exposing (..)


type Route
    = NotFound
    | LoginRoute
    | HomeRoute
    | ItemRoute (Maybe String)


reverseRoute : Route -> String
reverseRoute route =
    case route of
        NotFound ->
            "/"

        LoginRoute ->
            "/login"

        HomeRoute ->
            "/"

        ItemRoute (Just uid) ->
            "/items/" ++ uid

        ItemRoute Nothing ->
            "/items/new"


parseRoute : Url -> Route
parseRoute location =
    let
        matchers =
            oneOf
                [ map HomeRoute
                    top
                , map LoginRoute
                    (s "login")
                , map (ItemRoute Nothing)
                    (s "items" </> s "new")
                , map (ItemRoute << Just)
                    (s "items" </> string)
                ]
    in
    parse matchers location |> Maybe.withDefault NotFound


goTo : Key -> Route -> Cmd msg
goTo key route =
    Browser.Navigation.pushUrl key (reverseRoute route)
