module OutMsg exposing (OutMsg(..))

import Http


type OutMsg
    = OutNop
    | OutHttpError Http.Error
    | OutLogin { email : String, token : String }
    | OutLogOut
