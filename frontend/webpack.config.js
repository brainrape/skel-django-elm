const webpack = require('webpack')
const path = require('path')

const DEBUG = !! process.env.DEBUG
const elmFiles = [
    path.resolve(__dirname, "src/Main.elm")
]

module.exports = {
  mode: "development",
  module: {
    rules: [{
        test: /\.(css|scss)$/,
        loaders: [
            'style-loader',
            'css-loader',
        ]
    },
    {
        test: /\.html$/,
        exclude: /node_modules/,
        loader: 'file-loader?name=[name].[ext]'
      },
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        loader: "elm-webpack-loader",
        options: {
          debug: DEBUG,
          optimize: !DEBUG,
          files: elmFiles
        }
      }
    ]
  },
  devServer: {
    inline: true,
    historyApiFallback: true,
    stats: 'errors-only'
  },
  resolve: {
    extensions: ['.js'],
    modules : [
      process.env.NODE_PATH.split(":").filter((x) => x.includes("webpack-dev-server"))[0].concat("/webpack-dev-server/node_modules"),
      process.env.NODE_PATH.split(":").filter((x) => x.includes("-node-webpack-"))[0]
    ]
  },
  plugins : [
      new webpack.EnvironmentPlugin(['SERVER_URL'])
  ]
};
